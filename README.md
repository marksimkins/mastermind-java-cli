# Mastermind Game (Java CLI)

This is a very simple CLI version of the [Mastermind](https://en.wikipedia.org/wiki/Mastermind_%28board_game%29) code 
breaking game, written in Java mainly for fun, but also as a TDD exercise.

This was entirely done in one short day, so while the commandline interface is a little rough, it works.

I'm sure I'll tweak and improve this over time, and maybe even add a proper GUI.
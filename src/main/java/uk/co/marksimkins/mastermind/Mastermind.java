package uk.co.marksimkins.mastermind;

import uk.co.marksimkins.mastermind.models.Feedback;
import uk.co.marksimkins.mastermind.models.Pattern;
import uk.co.marksimkins.mastermind.models.Peg;

import java.util.*;

/**
 * The Mastermind! This is a simple code breaking game: https://en.wikipedia.org/wiki/Mastermind_%28board_game%29
 * <p>
 * This class handles the core logic, storing the codemaker's pattern, tracking the number of moves, making the
 * comparison to the guess, and returning a suitable response.
 * <p>
 * This was written using TDD.
 */
class Mastermind {

    private final Pattern codemakerPattern;
    private int moveCount;

    /**
     * Start a new game of Mastermind as the guesser, with a randomly generated codemaker pattern.
     */
    Mastermind() {
        this.codemakerPattern = Pattern.createRandom();
    }

    /**
     * Start a new game of Mastermind as the codemaker, with a provided codemaker pattern.
     */
    Mastermind(Pattern codemakerPattern) {
        this.codemakerPattern = codemakerPattern;
    }

    /**
     * @return The codemaker's pattern.
     */
    public Pattern getPattern() {
        return codemakerPattern;
    }

    /**
     * Make a guess of what the codemaker's pattern is.
     *
     * @param codebreakerGuess The guessed pattern.
     * @return A {@link Feedback} object describing whether it's correct, and if not, giving suitable peg hints.
     */
    public Feedback makeGuess(Pattern codebreakerGuess) {
        moveCount++;
        return new Feedback(compareGuess(codebreakerGuess));
    }

    /**
     * @return How many guess attempts have been made in this game.
     */
    public int getMoveCount() {
        return moveCount;
    }

    /*
     * This is the meat of the game, where the guess is compared and the response pegs generated.
     */
    private List<Peg> compareGuess(Pattern codebreakerGuess) {
        List<Peg> codebreakerColours = new ArrayList<>(codebreakerGuess.getPegs());
        List<Peg> feedbackPegs = new ArrayList<>();

        for (int i=0; i<codemakerPattern.getPegs().size(); i++) {
            Peg codemakerPeg = codemakerPattern.getPegs().get(i);
            Peg codebreakerPeg = codebreakerGuess.getPegs().get(i);

            if (codebreakerPeg.equals(codemakerPeg) && codebreakerColours.contains(codemakerPeg)) {
                feedbackPegs.add(Peg.BLACK);
                codebreakerColours.remove(codemakerPeg);
            } else {

                if (codebreakerGuess.getPegs().contains(codemakerPeg) && codebreakerColours.contains(codemakerPeg)) {
                    feedbackPegs.add(Peg.WHITE);
                    codebreakerColours.remove(codemakerPeg);
                } else {
                    feedbackPegs.add(Peg.EMPTY);
                }
            }
        }
        return Collections.unmodifiableList(feedbackPegs);
    }

}

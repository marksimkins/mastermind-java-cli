package uk.co.marksimkins.mastermind;

import uk.co.marksimkins.mastermind.models.Feedback;
import uk.co.marksimkins.mastermind.models.Pattern;
import uk.co.marksimkins.mastermind.models.Peg;

import java.util.Random;
import java.util.Scanner;

/**
 * The main CLI Application to play Mastermind!
 */
public class Application {

    private static final Random rnd = new Random();

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Mastermind mastermind = new Mastermind();

        System.out.println("");
        System.out.println("========================== Welcome to Mastermind! ==========================");
        System.out.println("=== I have chosen my code! What are you guessing?");
        System.out.println("===");
        System.out.println("=== Available options are: " + Peg.allToString());
        System.out.println("=== So you may guess, for example: '" + examplePattern() + "'");
        System.out.println("===");
        System.out.println("=== (Unknown choices will be considered as Empty pegs)");
        System.out.println("=== (Type 'exit' to exit, or 'giveup' if you give up and want to know what my code was)");
        System.out.println("===");
        printCursor();

        boolean endGame = false;

        while(scanner.hasNext()) {
            String input = scanner.nextLine();

            if (input.equals("exit")) {
                break;
            }

            if (input.equals("giveup")) {
                System.out.println("=== Aww, too hard for you? Ok, my code was: " + mastermind.getPattern().displayFull());
                System.out.println("=== Do you want to play again? (yes|no)");
                endGame = true;
                continue;
            }

            if (endGame) {

                if ("yes".equalsIgnoreCase(input)) {
                    mastermind = new Mastermind();
                    System.out.println("=== Ok, great! I've chosen a new code, what's your guess?");
                    System.out.println("=== (As a reminder, your available options are: " + Peg.allToString() + ")");
                    printCursor();
                    endGame = false;
                    continue;
                } else if ("no".equalsIgnoreCase(input)) {
                    break;
                }
                System.out.println("=== Sorry, I didn't quite get that, would you like to play again? (yes|no)");
                printCursor();
                continue;
            }
            Pattern guess = Pattern.fromString(input);
            Feedback feedback = mastermind.makeGuess(guess);

            if (feedback.isMatch()) {
                System.out.println("");
                System.out.println("=== Congratulations, you have guessed correctly! It took you " + mastermind.getMoveCount() + " tries.");
                System.out.println("=== My pattern is indeed " + guess.displayFull());
                System.out.println("=== Do you want to play again? (yes|no)");
                endGame = true;
            } else {
                System.out.println("");
                System.out.println("=== " + randomiseIncorrectSentence());
                System.out.println("=== Here is my feedback on your guess: ");
                feedback.println("===     ");
                System.out.println("=== Have another guess (your " + numberSuffix(mastermind.getMoveCount() + 1) + " time)");
            }
            printCursor();
        }

        System.out.println("=== Thanks for playing! Have a great day :)");
        System.out.println("");
    }

    private static void printCursor() {
        System.out.print("==> ");
    }

    /**
     * @return A random "You're wrong!" sentence, why not.
     */
    private static String randomiseIncorrectSentence() {
        switch (rnd.nextInt(3)) {
            case 0:
                return "Oh I'm sorry, that's not quite correct.";
            case 1:
                return "That's not correct I'm afraid!";
            case 2:
                return "Aah, not quite, sorry!";
        }
        return "Not this time!";
    }

    /**
     * Get the suffix for a number, e.g. 1st, 2nd, 3rd etc.
     *
     * @param n The number to get the suffix for.
     * @return  The appropriate String suffix.
     */
    private static String numberSuffix(int n){
        if (n % 100 / 10 == 1) {
            return n + "th";
        }
        switch (n % 10) {
            case 1: return n + "st";
            case 2: return n + "nd";
            case 3: return n + "rd";
            default: return n + "th";
        }
    }

    /**
     * @return A random example pattern for display.
     */
    private static String examplePattern() {
        Pattern examplePattern = Pattern.createRandom();
        return examplePattern.displayShort();
    }

}

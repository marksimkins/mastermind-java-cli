package uk.co.marksimkins.mastermind.models;

import java.util.Arrays;

/**
 * The Mastermind game is played with these pegs.
 * <p>
 * The minimum set required to play is Black, White, and Empty, as these are used in the guess feedback.
 * However more may be added for the guesses themselves to increase the difficulty of the game.
 */
public enum Peg {
    BLACK("B"),
    WHITE("W"),
    RED("R"),
    GREEN("G"),
    ORANGE("O"),
    YELLOW("Y"),
    EMPTY("E");

    private final String key;

    Peg(String key) {
        this.key = key;
    }

    public String toString() {
        StringBuilder response = new StringBuilder(this.name().toLowerCase());
        response.setCharAt(0, Character.toUpperCase(response.charAt(0)));
        response.append(String.format("(%s)", key));
        return response.toString();
    }

    /**
     * @return The key corresponding to this Peg. Used for commandline input and display.
     */
    public String getKey() {
        return key;
    }

    /**
     * Given some input, get a suitable peg matching the key.
     *
     * @param key The identifier for a Peg, either the short key, or the peg name.
     * @return The appropriate Peg matching the key, or Empty if none could be found.
     */
    public static Peg get(String key) {
        return Arrays.stream(values()).filter(peg -> peg.key.equalsIgnoreCase(key) || peg.name().equalsIgnoreCase(key))
                .findFirst().orElse(Peg.EMPTY);
    }

    /**
     * @return All of these Pegs as a nicely rendered displayable String.
     */
    public static String allToString() {
        StringBuilder sb = new StringBuilder();

        for (Peg peg : values()) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(peg.toString());
        }

        return sb.toString();
    }
}

package uk.co.marksimkins.mastermind.models;

import java.util.*;

/**
 * This Pattern is used to represent of up to 4 pegs, whether it's the codemaker's initial Pattern, or the
 * codebreaker's guess.
 */
public class Pattern {

    private static final Random rnd = new Random();
    private final List<Peg> pegs;

    /**
     * @return A random Pattern, useful for starting a new game against the computer.
     */
    public static Pattern createRandom() {
        Peg peg1 = tryForNonEmptyRandomPeg();
        Peg peg2 = tryForNonEmptyRandomPeg();
        Peg peg3 = tryForNonEmptyRandomPeg();
        Peg peg4 = tryForNonEmptyRandomPeg();
        return new Pattern(peg1, peg2, peg3, peg4);
    }

    /**
     * Make it less likely that an empty peg will be chosen, as it's a bit lame.
     * @return A random Peg, that's less likely to be Empty.
     */
    private static Peg tryForNonEmptyRandomPeg() {
        Peg peg = Peg.EMPTY;
        int tries = 0;
        while (peg.equals(Peg.EMPTY) || tries < 10) {
            peg = Peg.values()[rnd.nextInt(Peg.values().length)];
            tries++;
        }
        return peg;
    }

    /**
     * Create a Pattern from the input provided by the player.
     *
     * @param input A simple space-separated input of keys matching pegs.
     * @return A {@link Pattern} object matching the provided input. If input is incomplete or invalid,
     * EMPTY pegs will be returned where appropriate.
     */
    public static Pattern fromString(String input) {
        String[] keys = input.replaceAll(" +", " ").split(" ");
        // May try patterns with more than 4 pegs at some point, but not now
        Peg[] pegs = new Peg[4];
        for (int i=0; i<pegs.length; i++) {
            Peg peg = Peg.EMPTY;
            if (keys.length > i) {
                peg = Peg.get(keys[i].trim());
            }
            pegs[i] = peg;
        }
        return new Pattern(pegs[0], pegs[1], pegs[2], pegs[3]);
    }


    /**
     * Create a new Pattern with four Pegs.
     */
    public Pattern(Peg peg1, Peg peg2, Peg peg3, Peg peg4) {
        pegs = List.of(peg1, peg2, peg3, peg4);
    }

    /**
     * @return The unmodifiable {@link List} of {@link Peg}s in this {@link Pattern}.
     */
    public List<Peg> getPegs() {
        return pegs;
    }

    /**
     * @return A short displayable String description of this pattern, using space-separated keys. (e.g. 'W W W W')
     */
    public String displayShort() {
        StringBuilder sb = new StringBuilder();
        for (Peg peg : getPegs()) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(peg.getKey());
        }
        return sb.toString();
    }

    /**
     * @return A displayable String description of this pattern, with name and key. (e.g. 'White(W) White(W) White(W) White(W)')
     */
    public String displayFull() {
        StringBuilder sb = new StringBuilder();
        for (Peg peg : getPegs()) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(peg);
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pattern pattern = (Pattern) o;
        return Objects.equals(pegs, pattern.pegs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pegs);
    }

    @Override
    public String toString() {
        return "Pattern{" +
                "pegs=" + pegs +
                '}';
    }
}

package uk.co.marksimkins.mastermind.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Core to this game is the feedback given to incorrect pattern guesses.
 * <p>
 * <ul>
 *  <li>BLACK: this peg is the correct colour in the correct position.</li>
 *  <li>WHITE: there is another peg of this colour, but not in this position.</li>
 *  <li>EMPTY: this is incorrect, and there are no more pegs of this colour.</li>
 * </ul>
 */
public class Feedback {

    private final List<Peg> pegs;

    public Feedback(List<Peg> pegs) {
        // "Sort" this combination of pegs to put BLACK pegs first, then WHITE, then EMPTY. Otherwise it's too easy,
        // and corresponds to the exact positions of the guess/code.
        List<Peg> sortedPegs = new ArrayList<>();
        addMatchingPegs(sortedPegs, pegs, Peg.BLACK);
        addMatchingPegs(sortedPegs, pegs, Peg.WHITE);
        addMatchingPegs(sortedPegs, pegs, Peg.EMPTY);
        // Now make the list unmodifable.
        this.pegs = Collections.unmodifiableList(sortedPegs);
    }

    /**
     * Helper method to only add those pegs in the list to the internal list if it matches the type specified.
     * @param sortedPegs The final {@link List} of sorted pegs you want to hold internally.
     * @param pegs    The {@link List} of pegs provided to this class.
     * @param toMatch The type of {@link Peg} we want to add to our internal list.
     */
    private static void addMatchingPegs(List<Peg> sortedPegs, List<Peg> pegs, Peg toMatch) {
        for (Peg peg : pegs) {
            if (peg.equals(toMatch)) {
                sortedPegs.add(toMatch);
            }
        }
    }

    /**
     * @return Simple boolean response as to whether this set of pegs indicates a full match.
     */
    public boolean isMatch() {
        return pegs.stream().allMatch(p -> p == Peg.BLACK);
    }

    /**
     * @return Get the pegs described in this feedback.
     */
    public List<Peg> getPegs() {
        return pegs;
    }

    /**
     * Print out this Feedback in the traditional square, with any provided indent (to look nice), e.g. with a
     * "===&nbsp;&nbsp;&nbsp;" indent:
     *
     * <pre>
     * ===   B B
     * ===   W E
     * </pre>
     * @param indent Any indent you want to use to make this look nice in the commandline.
     */
    public void println(String indent) {
        StringBuilder line = new StringBuilder();
        for (int i=0; i<pegs.size(); i++) {
            if (i == 2) {
                System.out.println(indent + line.toString());
                line = new StringBuilder();
            }
            if (line.length() > 0) {
                line.append(" ");
            }
            line.append(pegs.get(i).getKey());
        }
        System.out.println(indent + line.toString());
    }

}

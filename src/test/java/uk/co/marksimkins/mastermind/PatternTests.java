package uk.co.marksimkins.mastermind;

import org.junit.jupiter.api.Test;
import uk.co.marksimkins.mastermind.models.Pattern;
import uk.co.marksimkins.mastermind.models.Peg;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class PatternTests {

    @Test
    void patternIsGenerated() {
        Pattern pattern = Pattern.createRandom();
        System.out.println(pattern);
        Set<Peg> availablePegs = Set.of(Peg.values());
        for (Peg patternPeg : pattern.getPegs()) {
            assertNotNull(patternPeg);
            assertTrue(availablePegs.contains(patternPeg));
        }
    }

    @Test
    void patternIsCreatedFromString() {
        Pattern pattern = Pattern.fromString("B W E B");

        assertEquals(pattern.getPegs().get(0), Peg.BLACK);
        assertEquals(pattern.getPegs().get(1), Peg.WHITE);
        assertEquals(pattern.getPegs().get(2), Peg.EMPTY);
        assertEquals(pattern.getPegs().get(3), Peg.BLACK);

        pattern = Pattern.fromString("B W  E B");

        assertEquals(pattern.getPegs().get(0), Peg.BLACK);
        assertEquals(pattern.getPegs().get(1), Peg.WHITE);
        assertEquals(pattern.getPegs().get(2), Peg.EMPTY);
        assertEquals(pattern.getPegs().get(3), Peg.BLACK);

        pattern = Pattern.fromString("B W E B A B C");

        assertEquals(pattern.getPegs().get(0), Peg.BLACK);
        assertEquals(pattern.getPegs().get(1), Peg.WHITE);
        assertEquals(pattern.getPegs().get(2), Peg.EMPTY);
        assertEquals(pattern.getPegs().get(3), Peg.BLACK);

        pattern = Pattern.fromString("B w E");

        assertEquals(pattern.getPegs().get(0), Peg.BLACK);
        assertEquals(pattern.getPegs().get(1), Peg.WHITE);
        assertEquals(pattern.getPegs().get(2), Peg.EMPTY);
        assertEquals(pattern.getPegs().get(3), Peg.EMPTY);

        pattern = Pattern.fromString("B z");

        assertEquals(pattern.getPegs().get(0), Peg.BLACK);
        assertEquals(pattern.getPegs().get(1), Peg.EMPTY);
        assertEquals(pattern.getPegs().get(2), Peg.EMPTY);
        assertEquals(pattern.getPegs().get(3), Peg.EMPTY);

        pattern = Pattern.fromString("abcde.");

        assertEquals(pattern.getPegs().get(0), Peg.EMPTY);
        assertEquals(pattern.getPegs().get(1), Peg.EMPTY);
        assertEquals(pattern.getPegs().get(2), Peg.EMPTY);
        assertEquals(pattern.getPegs().get(3), Peg.EMPTY);

        pattern = Pattern.fromString("");

        assertEquals(pattern.getPegs().get(0), Peg.EMPTY);
        assertEquals(pattern.getPegs().get(1), Peg.EMPTY);
        assertEquals(pattern.getPegs().get(2), Peg.EMPTY);
        assertEquals(pattern.getPegs().get(3), Peg.EMPTY);
    }

    @Test
    void patternIsCreatedFromEnum() {
        Pattern pattern = new Pattern(Peg.WHITE, Peg.BLACK, Peg.WHITE, Peg.BLACK);

        assertEquals(pattern.getPegs().get(0), Peg.WHITE);
        assertEquals(pattern.getPegs().get(1), Peg.BLACK);
        assertEquals(pattern.getPegs().get(2), Peg.WHITE);
        assertEquals(pattern.getPegs().get(3), Peg.BLACK);
    }

    @Test
    void patternSupportsEmptyHoles() {
        Pattern pattern = new Pattern(Peg.WHITE, Peg.EMPTY, Peg.WHITE, Peg.BLACK);

        assertEquals(pattern.getPegs().get(0), Peg.WHITE);
        assertEquals(pattern.getPegs().get(1), Peg.EMPTY);
        assertEquals(pattern.getPegs().get(2), Peg.WHITE);
        assertEquals(pattern.getPegs().get(3), Peg.BLACK);
    }

    @Test
    void patternsMatchCorrectly() {
        Pattern codemakerPattern = new Pattern(Peg.WHITE, Peg.BLACK, Peg.WHITE, Peg.BLACK);
        Pattern codebreakerGuess = new Pattern(Peg.WHITE, Peg.BLACK, Peg.WHITE, Peg.BLACK);

        assertEquals(codemakerPattern, codebreakerGuess);
    }

    @Test
    void patternsDoNotMatchCorrectly() {
        Pattern codemakerPattern = new Pattern(Peg.WHITE, Peg.BLACK, Peg.WHITE, Peg.BLACK);
        Pattern codebreakerGuess = new Pattern(Peg.WHITE, Peg.WHITE, Peg.WHITE, Peg.BLACK);

        assertNotEquals(codemakerPattern, codebreakerGuess);
    }

}

package uk.co.marksimkins.mastermind;

import org.junit.jupiter.api.Test;
import uk.co.marksimkins.mastermind.models.Peg;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PegTests {

    @Test
    void keyMatchesPeg() {
        Peg peg = Peg.get("W");
        assertEquals(peg, Peg.WHITE);

        peg = Peg.get("B");
        assertEquals(peg, Peg.BLACK);

        peg = Peg.get("E");
        assertEquals(peg, Peg.EMPTY);
    }

    @Test
    void keyDoesNotMatchPeg() {
        Peg peg = Peg.get("Z");
        assertEquals(peg, Peg.EMPTY);
    }

    @Test
    void keyToStringIsCorrect() {
        Peg peg = Peg.WHITE;
        assertEquals(peg.toString(), "White(W)");

        peg = Peg.EMPTY;
        assertEquals(peg.toString(), "Empty(E)");
    }

    @Test
    void allPegsToStringIsCorrect() {
        String allPegs = Peg.allToString();

        StringBuilder sb = new StringBuilder();
        for (Peg peg : Peg.values()) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(peg.toString());
        }

        assertEquals(allPegs, sb.toString());
    }

}

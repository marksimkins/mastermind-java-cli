package uk.co.marksimkins.mastermind;

import org.junit.jupiter.api.Test;
import uk.co.marksimkins.mastermind.models.Feedback;
import uk.co.marksimkins.mastermind.models.Pattern;
import uk.co.marksimkins.mastermind.models.Peg;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MastermindTests {

    @Test
    void matchingFeedbackIsCorrect() {
        Pattern codemakerPattern = new Pattern(Peg.WHITE, Peg.BLACK, Peg.WHITE, Peg.BLACK);
        Pattern codebreakerGuess = new Pattern(Peg.WHITE, Peg.BLACK, Peg.WHITE, Peg.BLACK);

        Mastermind mastermind = new Mastermind(codemakerPattern);
        Feedback feedback = mastermind.makeGuess(codebreakerGuess);

        assertTrue(feedback.isMatch());
        assertEquals(feedback.getPegs().get(0), Peg.BLACK);
        assertEquals(feedback.getPegs().get(1), Peg.BLACK);
        assertEquals(feedback.getPegs().get(2), Peg.BLACK);
        assertEquals(feedback.getPegs().get(3), Peg.BLACK);
    }

    @Test
    void noColoursMatchFeedbackIsCorrect() {
        Pattern codemakerPattern = new Pattern(Peg.WHITE, Peg.BLACK, Peg.WHITE, Peg.BLACK);
        Pattern codebreakerGuess = new Pattern(Peg.EMPTY, Peg.EMPTY, Peg.EMPTY, Peg.EMPTY);

        Mastermind mastermind = new Mastermind(codemakerPattern);
        Feedback feedback = mastermind.makeGuess(codebreakerGuess);

        assertFalse(feedback.isMatch());
        assertEquals(feedback.getPegs().get(0), Peg.EMPTY);
        assertEquals(feedback.getPegs().get(1), Peg.EMPTY);
        assertEquals(feedback.getPegs().get(2), Peg.EMPTY);
        assertEquals(feedback.getPegs().get(3), Peg.EMPTY);
    }

    @Test
    void coloursMatchNotPositionFeedbackIsCorrect() {
        Pattern codemakerPattern = new Pattern(Peg.WHITE, Peg.BLACK, Peg.WHITE, Peg.BLACK);
        Pattern codebreakerGuess = new Pattern(Peg.BLACK, Peg.WHITE, Peg.EMPTY, Peg.EMPTY);

        Mastermind mastermind = new Mastermind(codemakerPattern);
        Feedback feedback = mastermind.makeGuess(codebreakerGuess);

        assertFalse(feedback.isMatch());
        assertEquals(feedback.getPegs().get(0), Peg.WHITE);
        assertEquals(feedback.getPegs().get(1), Peg.WHITE);
        assertEquals(feedback.getPegs().get(2), Peg.EMPTY);
        assertEquals(feedback.getPegs().get(3), Peg.EMPTY);
    }

    @Test
    void coloursMatchNotPositionMiddleFeedbackIsCorrect() {
        Pattern codemakerPattern = new Pattern(Peg.WHITE, Peg.BLACK, Peg.WHITE, Peg.BLACK);
        Pattern codebreakerGuess = new Pattern(Peg.EMPTY, Peg.WHITE, Peg.BLACK, Peg.EMPTY);

        Mastermind mastermind = new Mastermind(codemakerPattern);
        Feedback feedback = mastermind.makeGuess(codebreakerGuess);

        assertFalse(feedback.isMatch());
        assertEquals(feedback.getPegs().get(0), Peg.WHITE);
        assertEquals(feedback.getPegs().get(1), Peg.WHITE);
        assertEquals(feedback.getPegs().get(2), Peg.EMPTY);
        assertEquals(feedback.getPegs().get(3), Peg.EMPTY);
    }

    @Test
    void coloursMatchSomePositionNotTooManyFeedbackIsCorrect() {
        Pattern codemakerPattern = new Pattern(Peg.WHITE, Peg.BLACK, Peg.WHITE, Peg.BLACK);
        Pattern codebreakerGuess = new Pattern(Peg.WHITE, Peg.WHITE, Peg.WHITE, Peg.WHITE);

        Mastermind mastermind = new Mastermind(codemakerPattern);
        Feedback feedback = mastermind.makeGuess(codebreakerGuess);

        assertFalse(feedback.isMatch());
        assertEquals(feedback.getPegs().get(0), Peg.BLACK);
        assertEquals(feedback.getPegs().get(1), Peg.BLACK);
        assertEquals(feedback.getPegs().get(2), Peg.EMPTY);
        assertEquals(feedback.getPegs().get(3), Peg.EMPTY);
    }

    @Test
    void feedbackIsCorrect() {
        Pattern codemakerPattern = new Pattern(Peg.WHITE, Peg.ORANGE, Peg.YELLOW, Peg.GREEN);
        Pattern codebreakerGuess = new Pattern(Peg.GREEN, Peg.GREEN, Peg.GREEN, Peg.GREEN);

        Mastermind mastermind = new Mastermind(codemakerPattern);
        Feedback feedback = mastermind.makeGuess(codebreakerGuess);

        assertFalse(feedback.isMatch());
        assertEquals(feedback.getPegs().get(0), Peg.BLACK);
        assertEquals(feedback.getPegs().get(1), Peg.EMPTY);
        assertEquals(feedback.getPegs().get(2), Peg.EMPTY);
        assertEquals(feedback.getPegs().get(3), Peg.EMPTY);
    }

    @Test
    void moveCountIsCorrect() {
        Pattern codemakerPattern = new Pattern(Peg.WHITE, Peg.BLACK, Peg.WHITE, Peg.BLACK);
        Pattern codebreakerGuess = new Pattern(Peg.WHITE, Peg.WHITE, Peg.WHITE, Peg.WHITE);

        Mastermind mastermind = new Mastermind(codemakerPattern);
        assertEquals(mastermind.getMoveCount(), 0);

        mastermind.makeGuess(codebreakerGuess);
        assertEquals(mastermind.getMoveCount(), 1);

        mastermind.makeGuess(codebreakerGuess);
        assertEquals(mastermind.getMoveCount(), 2);
    }

}
